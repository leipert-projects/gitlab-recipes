---
title: Chilaquiles
from: jivanvl
image: https://assets.epicurious.com/photos/5a9ecb6a9d7f7965083147b4/6:4/w_274%2Ch_169/eggshop-chilaquiles-recipe-HC-030618.jpg
portions: 2
prepTime: 30 min
keywords:
  - Breakfast
  - Brunch
  - Simple
ingredients:
  - 500g Tortillas
  - 2 tomatoes
  - 1 and 1/2 gloves of garlic
  - 1/2 Jalapeño pepper
  - 2 tbsp of olive oil/vegetable oil
  - 2 cups of water
  - "Garnish: Cilantro/Coriander"
  - 1 egg **
  - 1/4 onion **
  - Sour cream **
---

1. Cut the tortillas in small chunks, the size can be in quarters, eights. This is down to your preference.
2. Boil the water and add the tomatoes and the jalapeño pepper. Take them out after 20 seconds or so.
3. To form the sauce mix some of the water with the tomatoes, garlic and jalapeño. 3/4 cup of water should suffice, and blend them.
4. In a deep pan at medium heat, add the oil and saute the tortilla chunks until they're toasty. Add the sauce afterwards.
5. Keep stirring until a big chunk of the sauce gets absorbed, once done it's time to serve. You can add some chopped onions, eggs, cream or all of them together. Garnish with some cilantro.

TIPS: 

* This recipe can be changed to your heart content, want gluten free tortillas? Sure, want to add meat or tofu? No problem
* The sauce (salsa) can be prepared with spiceless chilies, there are few them out there that just give a smokyness flavor sorta how pepper works, e.g. chocolate chillies
* Remove the seeds from really spicy chillies, it could ruin your day if you leave them there.

** optional ingredients for plating
