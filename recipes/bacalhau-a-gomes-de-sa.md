---
title: Bacalhau à Gomes de Sá
from: andr3
image: https://i.imgur.com/PmM1Pj4.jpg
portions: 6
prepTime: 40 min
keywords:
  - Cod
  - Fish
  - Oven
ingredients:
  - 1kg salted cod
  - 1kg potatoes
  - 2 cups milk
  - 3 onions, sliced
  - 3 cloves garlic, crushed
  - 4 hard-boiled eggs
  - 1/2 cup olive oil
  - 20 black olives
  - 1 bunch of parsley
  - salt
  - pepper
---

1. Soak the cod in water for 24 to 36 hours and change the water every 8 hours.
2. After cod is desalted, remove bones from cod and shred it in small pieces.
3. Bring milk to a boil then turn off the heat. Infuse cod pieces in hot milk for about 1 to 2 hours.
4. Peel potatoes and cook them in boiling water for 25 minutes.
5. Cut potatoes in ½ inch thick slices. Add salt and pepper.
6. Preheat oven to 375 F.
7. Meanwhile, add the onions in a hot skillet with a little olive oil. Sauté for about 5 to 10 minutes. Add the garlic and continue cooking for 2 minutes.
8. In a casserole dish, drizzle ¼ cup of olive oil.
9. Add layers of potatoes, drained cod and the onion/garlic mixture. Season with salt and pepper.
10. Drizzle another ¼ cup of olive oil on top.
11. Bake for 40 minutes.
12. Garnish with quartered or sliced hard-boiled eggs, chopped parsley and black olives.