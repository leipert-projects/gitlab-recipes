---
title: Roasted Vegetable Jalfrezi
from: samdbeckham
image: https://britishhypermarket.com/media/catalog/product/cache/1/image/9df78eab33525d08d6e5fb8d27136e95/S/G/SGN1222_1.jpg
portions: 3–4
prepTime: 30 min
keywords:
  - Dinner
  - Simple
  - Vegetarian
ingredients:
  - About 3 sweet potatoes (yams)
  - Some cauliflower
  - Half a box of chestnut cushrooms
  - Sumac
  - Curry sauce powder
  - Some kind of cooking oil
  - 225g rice
  - A couple of bay leaves
  - 500ml water
  - A jar of Loyd Grossman jalfrezi sauce
---

You might have noticed from the ingredient list that I'm not that fussed about measuring things.
This is what I make when I want something really tasty, but I'm not looking to impress anyone with my cooking.
I don't really time things either, just keep an eye on them and if they're burning, turn the heat down.

1. Preheat the oven to 200˚C. Or just as hot as it'll go, it doesn't really matter. 
2. Peel the sweet potatoes and chop them into little chunks, about the size of a dice.
3. Put your potato cubes on a baking tray, drizzle with a bit of oil, and put them in the oven.
4. Chop up your cauliflower into tiny little trees. About this big <--------------->
5. Take the tray back out of the oven and put the cauliflower on there too.
6. Sprinkle sumac on the taters and the curry sauce powder on the cauliflower. Then return the tray to the oven.
7. Put your rice in a pan of salted water and throw some bay leaves in to keep it company. Put it on a medium–high heat till it boils, then turn it down to a simmer.
8. Whilst everything else is still cooking, chop your mushroms into halves or quarters depending on how much you can still be bothered to chop things.
9. Pour a little more oil in a decent-sized frying pan and fry up them mushrooms. Salt and pepper them for extra chef points.
10. Pour your jar of Jalfrezi sauce over the mushrooms. We use a pre-made jar here for two reasons. It's easier, and it tastes better than any jalfreezi sauce I could make.
11. Your veggies are probably roasted by this point so chuck them in the pan with the mushroom jalfrezi. Simmer.
12. Tell the cat to get down off the worktop.
13. Wait till the rice is cooked, roughly 10 minutes after it started boiling.
14. Discard the bay leaves from the rice, drain and plate it all up.
