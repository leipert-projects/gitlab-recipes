---
title: Classic Mac and Cheese from scratch
from: johnrhampton
image: https://bit.ly/2RT7O6e
portions: 6
prepTime: 22 min
keywords:
  - Dinner
  - Simple
  - Cheese
ingredients:
  - 2 cups milk
  - 2 Tbsp. butter
  - 2 Tbsp. all-purpose flour
  - .5 tsp. salt
  - .25 tsp. freshly ground black pepper
  - 10 oz. block extra sharp cheddar cheese, shredded
  - .25 tsp. ground red pepper (optional)
  - 8 oz. elbow macaroni cooked
---

1.  **Whisk flour into butter.** Preheat oven to 400. Microwave milk at HIGH for 1.5 minutes. Melt butter in large skillet or Dutch over over medium-low heat; whisk in flour until smooth. Cook, whisking constantly, 1 minute.
2.  **Whisk in warm milk.** Gradually whisk in warm milk, and cook, whisking constantly, 5 minutes or until thickened.
3.  **Whisk in cheese.** Whisk in salt, black pepper, 1 cup shredded cheese, and, if desired, red pepper until smooth; stir in pasta. Spoon pasta mixture into a lightly greased 2-qt. baking dish; top with remaining cheese. Bake at 400 for 20 minutes or until golden and bubbly.

TIP: Replace the cheddar cheese with a cheese of your preference.
