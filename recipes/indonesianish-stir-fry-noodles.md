---
title: Indonesian-ish Stir-fry Noodles (top ramen stir fry)
from: dennis
image: ../images/indonesianish-stir-fry-noodles.jpg
portions: 2
prepTime: 15 min
keywords:
  - Breakfast
  - Lunch
  - Dinner
  - One of a million things to do with top ramen
ingredients:
  - 1-2 packets of Indomie (or similar brand) instant noodles
  - 1-2 eggs or other protein
  - Mangetout (peas in pods)
  - Paprika/peppers
  - Tofu, tomatoes, eggplant, or any other vegetables you like
  - Bali Kitchen sweet soy sauce (or similar)
  - Oil (I recommend coconut!)
---

1. Prepare and cut vegetables for stir fry.
1. Bring 500 ml of water to boil for noodles and protein.
1. Open seasoning packets from instant noodles and put into its own plate for later serving.
1. Put oil in wok, and stir fry vegetables to your liking. Add sweet soy sauce to taste.
1. At the same time, add noodles, eggs, and protein into boiling water and let cook for ~3 minutes.
1. Once stir fry is finished, keep on low heat while finishing the noodles.
1. Empty noodles and protein into strainer, and then put into plate with seasoning powder and mix well!
1. Place stir fry into separate plate, or simply on top of the noodles.
1. Enjoy!

![](../images/indonesianish-stir-fry-noodles.jpg)