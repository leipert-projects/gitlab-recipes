---
title: Guacamole
from: clemmakesapps
portions: 4
prepTime: 30 min
keywords:
  - Snack
ingredients:
  - 4 large avocados
  - 1 lime
  - 1 onion
  - 1 Tomato
  - Salt
---

1.  Cut the avocadoes and place them in a bowl
2.  Mash the avocadoes until it becomes one mixture
3.  Cut the lime in half and squeeze it into the mixture
4.  Add a pinch of salt and mix the mixture (add more salt and or lime based on personal taste)
5.  Take one layer of the onion and finely dice it (as small as you can)
6.  Remove the skin of the tomato and finely dice it (as small as you can)
7.  Add the diced onion and tomato, and mix it thoroughly to make your guacamole
