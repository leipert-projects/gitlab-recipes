---
title: Greiling Family Chili
from: mikegreiling
image: https://i.imgur.com/N17v7sb.jpg
portions: 10
prepTime: 30 min
keywords:
  - Dinner
  - Spicy
  - Hearty
  - Simple
ingredients:
  - 2 lbs ground beef
  - 1 large white onion (chopped)
  - 1 12oz bottle of dark beer (e.g. Newcastle)
  - red bell pepper (chopped)
  - 4 minced garlic cloves
  - 2 tablespoons chili powder
  - 1/2 teaspoon salt
  - 2 teaspoons ground cumin
  - 1 teaspoon dried oregano
  - 1/4 teaspoon cayenne
  - 2 cans kidney beans (~15.5oz cans)
  - 1 can black beans (~15.5oz can)
  - 1 28oz can diced tomatoes
  - 1 6oz can tomato paste
  - Tobasco sauce
  - 1 4oz can chopped jalapeños (or fresh when available)
  - teaspoon sugar
  - shredded cheese (cheddar jack is best)
  - sour cream
  - white rice (optional)
  - saltine crackers (optional)
---

1. Cook the ground beef in a large frying pan. Stir and chop with spatula to crumble until brown and cooked through (~7 min)
2. Add the chili powder, cayenne, onion, salt, cumin, oregano, garlic, and a splash of tabasco sauce (vary depending on how spicy you like it).
3. Cook while stirring, until onion is translucent (~5 min).
4. Transfer to large pot. Add the canned beans (juice and all), canned tomatoes (juice and all), tomato paste, bell pepper,jalapenos, sugar, and beer. Stir well.
5. Bring to a boil.
6. Lower the heat to medium-low and simmer, uncovered, for 30 min, stirring occasionally.
7. Top individual bowls of chili with a dollop of sour cream and a generous sprinkle of cheese.

TIP: I like serving over white rice and/or topping with crumbled saltine crackers. Also goes well with corn bread.
