tile: Sushi Bowl
from: shampton
image: https://farm8.staticflickr.com/7308/13949625000_b1023215bb_o.jpg
portions: 1
prepTime: 30 min
keywords:
  - Dinner
  - Seafood
  - Simple
ingredients:
  - 1 cup of cooked rice (white or brown)
  - 1 cucumber, sliced and cubed
  - 1/4 red onion, sliced
  - Smoked salmon (I prefer cooked), broken into smaller pieces
  - 1 avocado, cubed
  - Soy sauce
---

1. Cook your choice of rice.
2. If you choose, cook your salmon.
3. Put the ingredients in a bowl with rice on the bottom.
4. Love your meal.
5. Eat your meal.