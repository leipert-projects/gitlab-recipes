---
title: Anda Bhurji (Indian Scrambled Eggs)
from: kushalpandya
image: https://i.imgur.com/zZXRGWO.jpg
portions: 2
prepTime: 30 min
keywords:
  - Breakfast
  - Snack
ingredients:
  - 4 eggs
  - 1 medium red onion
  - 1 medium bell pepper (or capsicum)
  - 1 medium tomato
  - 1 green chilly
  - 1 small bowl of fine chopped coriander (for dressing)
  - 2 tablespoons cooking oil
  - 2 tablespoons butter
  - 1 tablespoon cumin seeds
  - 1 tablespoon chilly flakes
  - 1 teaspoon mustard seeds [Optional]
  - 1 teaspoon of Ajwain (aka Bishop's weed) [Optional]
  - 1 teaspoon of Turmeric
  - Salt to taste
---

1. Fine chop Onions and keep it in separate bowl, fine chop tomato, bell pepper and green chilly and put all those in another bowl.
2. Take all 4 eggs, break and whisk them well in a bowl, you can add a teaspoon of milk or water for fluffy results when cooked. Also add slight salt to the mixture for even taste.
3. Take a frying pan, add cooking oil and give it a few minutes till it starts warming up.
4. Add cumin seeds, mustard seeds and Ajwain to oil and stir.
5. Once you get the fragrance of all spices, add onions and chilly flakes and stir well until onions turn pink (about 5-7 minutes).
6. Add other veggies that we chopped earlier (tomato, bell pepper and green chilly) and Turmeric and stir for another 10 minutes.
7. Add whisked eggs mixture and start stirring gently so that it can cover all the veggies, keep stirring for about another 10 minutes.
8. Once you see the eggs almost cooked, add butter and keep stirring. At this point you can also add salt to adjust the taste to your liking.
9. Use spatula to crush the scramble within pan for even texture while stirring.
10. Once ready, dress with coriander.

Serve it hot with toasted bread or baked beans or even potato fries.

Pro Tip: You can be creative with veggies you use and add even lattuce, mushroom, green beans, spring onions, etc.