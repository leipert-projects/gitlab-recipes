---
title: Midyett Rub (Perfect on beef)
from: timzallmann
portions: 800g meat
prepTime: 5 min (days to get Sumac)
keywords:
  - Dinner
ingredients:
  - 2 parts good sea salt
  - 1 part black pepper
  - 1/4-1/2 part sumac
  - 1/3-1/2 part ground coffee
  - 1/4-1/2 part garlic powder
  - 1/4-1/2 part cocoa powder
---

1.  Mix the midyett ruba based on the recipe (its a dry rub)
2.  Put it on the piece of beef you have
3.  Let it sink in for 10 minutes
4.  Get your grill super hot and grill the meat as wished