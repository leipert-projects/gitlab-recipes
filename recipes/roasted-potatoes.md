---
title: Bratkartoffeln (roasted potato)
from: leipert
image: https://static.essen-und-trinken.de/bilder/ba/cd/17250/galleryimage/7d79385ef546f6e2f43389b855e9d057.jpg
portions: 2
prepTime: 30 min
keywords:
  - Breakfast
  - Dinner
  - Simple
ingredients:
  - 400g potatoes
  - 80g bacon/ham cubes
  - 1 onion
  - 2 tbsp. Butter/Oil
  - "Garnish: Parsley"
---

1.  Dice the onion (and the bacon if not already diced). Heat 1 tbsp in a pan. Fry the bacon/ham until crispy. Add onion for a minute. Remove both from the pan.
2.  Cut the potatoes into 3-5 mm thick slices. Works best with boiled potatoes from the day before. Roast potatoes 10-20 minutes on medium heat until they are brownish to taste.
3.  Re-add the onion and bacon, roast for 2 minutes. Mix in some parsley before serving.

TIP: Goes great together with sunny-side-up eggs or scrambled eggs