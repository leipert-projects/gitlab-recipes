---
title: Buttermilk blueberry pancakes
from: wortschi
image: https://imgur.com/a/7CfiCBH
portions: 3-4
prepTime: 25 min
keywords:
  - Breakfast
  - Sweet
  - Simple
ingredients:
  - 25g butter
  - 2 eggs
  - 200ml buttermilk
  - 1-2 tbsp. sugar
  - 150g flour (I prefer whole wheat flour)
  - 1 tsp. baking powder
  - pinch of salt
  - ~200-250g blueberries
---

1. Melt butter in a pan.
2. Combine eggs, buttermilk, sugar and melted butter in a large bowl.
3. Add flour, pinch of salt and baking powder. Mix well.
4. Cover with a lid and let it rest for 15min.
5. Add blueberries to the dough.
6. Add a little bit of butter to a medium hot pan. Use about 2 tbsp. dough for each pancake. Brown on both sides and serve hot.
7. Keep warm in oven at 60°C.

TIP: Instead of blueberries, I like to add bananans instead. I like serving with greek yoghurt, fresh berries and maple syrup.
