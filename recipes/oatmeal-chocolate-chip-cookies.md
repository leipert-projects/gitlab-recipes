---
title: Oatmeal Chocolate Chip Cookies
from: annabeldunstone
image: ../images/oatmeal-choc-chip.jpg
portions: dunno
prepTime: dunno
keywords:
  - Dessert
  - Cookies
ingredients:
  - 1 cup butter (softened to room temperature)
  - 1 cup packed light brown sugar
  - 1/2 cup granulated sugar
  - 2 eggs (room temperature)
  - 2 teaspoons vanilla extract
  - 1 1/4 cups all-purpose flour
  - 1/2 teaspoon baking soda
  - 1 teaspoon salt
  - 1/2 teasoon corn starch
  - 3 cups quick-cooking oats
  - 1 cup semi-sweet chocolate chips
  - 1/2 cup Heath bits 'o brickle
  - 1/2 cup white chocolate chips
---

1. In large bowl, cream together butter, brown sugar, and white sugar until light and fluffy
2. Beat in eggs, one at a time, and stir in vanilla
3. In medium bowl, combine flour, baking soda, salt, and corn starch
4. Stir dry ingredients into wet ingredients until just blended
5. Mix in oats and chocolate chips
6. Allow to chill in refridgerator overnight
7. Line cookie sheets with parchment paper, and bake cookies at 325ºF for 10-12 minutes
8. Allow to cool on baking sheet for 5 minutes then transfer to wire rack to cool completely
